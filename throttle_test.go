package throttling_func

import (
	"fmt"
	"testing"
	"time"
)

func TestThrottler_Run(t1 *testing.T) {
	type fields struct {
		t           chan struct{}
		done        chan bool
		mainFunc    func()
		defaultFunc func()

		ticker *time.Ticker
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "Only 1 user request per second",
			fields: fields{
				ticker: time.NewTicker(time.Second / 1),
				t:      make(chan struct{}, 1),
				done:   make(chan bool, 1),
				mainFunc: func() {
					fmt.Printf("%s: request from IP: 10.1.1.1 processed\n", time.Now().String())
				},
				defaultFunc: func() {
					fmt.Printf("%s: just walking around\n", time.Now().String())
				},
			},
		},
		{
			name: "Only 3 failed user transactions per Day",
			fields: fields{
				ticker: time.NewTicker(24 * time.Hour / 3),
				t:      make(chan struct{}, 3),
				done:   make(chan bool, 1),
				mainFunc: func() {
					fmt.Printf("%s: User failed Transaction processed\n", time.Now().String())
				},
				defaultFunc: func() {
					fmt.Printf("%s: just walking around\n", time.Now().String())
				},
			},
		},
		{
			name: "Only 12 user accounts per day from specific IP address",
			fields: fields{
				ticker: time.NewTicker(24 * time.Hour / 12),
				t:      make(chan struct{}, 12),
				done:   make(chan bool, 1),
				mainFunc: func() {
					fmt.Printf("%s: Account from IP: 10.1.1.1 address processed\n", time.Now().String())
				},
				defaultFunc: func() {
					fmt.Printf("%s: just walking around\n", time.Now().String())
				},
			},
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Throttler{
				ticker:      tt.fields.ticker,
				t:           tt.fields.t,
				done:        tt.fields.done,
				mainFunc:    tt.fields.mainFunc,
				defaultFunc: tt.fields.defaultFunc,
			}
			t.Run()
			/*
			 * 5 sec interval for testing & Stop
			 */
			time.Sleep(5 * time.Second)
			t.Stop()
		})
	}
}
