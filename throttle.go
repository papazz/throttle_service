package throttling_func

import (
	"time"
)

type Throttler struct {
	/*
	 * t for throttle chan
	 */
	t           chan struct{}
	mainFunc    func()
	defaultFunc func()
	/*
	 * done as stop mechanism
	 */
	done chan bool
	/*
	 * ticker as distribution
	 */
	ticker *time.Ticker
}

func (t *Throttler) sync() {
	defer t.ticker.Stop()
	for {
		select {
		case <-t.done:
			return
		case <-t.ticker.C:
			t.t <- struct{}{}
		default:
		}
	}
}

func (t *Throttler) receive() {
	for {
		select {
		case <-t.done:
			return
		case <-t.t:
			/*
			 * call main func (ex. HTTPRequest)
			 */
			t.mainFunc()
		default:
			/*
			 * t.defaultFunc()
			 */
		}
	}
}

func (t *Throttler) Run() {
	go t.sync()
	go t.receive()
}

func (t *Throttler) Stop() {
	t.done <- true
}

func NewThrottler(
	maxEvents int,
	duration time.Duration,
	pe func(),
	pd func()) *Throttler {
	return &Throttler{
		ticker:      time.NewTicker(duration),
		t:           make(chan struct{}, maxEvents),
		done:        make(chan bool, 1),
		mainFunc:    pe,
		defaultFunc: pd,
	}
}
